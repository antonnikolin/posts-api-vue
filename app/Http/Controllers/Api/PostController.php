<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostStoreRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $orderColumn = request('order_column', 'created_at');
        if (!in_array($orderColumn, ['id', 'title', 'created_at'])) {
            $orderColumn = 'created_at';
        }
        $orderDirection = request('order_direction', 'desc');
        if (!in_array($orderDirection, ['asc', 'desc'])) {
            $orderDirection = 'desc';
        }

        return PostResource::collection( Post::with('category')
            ->filter()
            ->orderBy($orderColumn, $orderDirection)
            ->paginate(50));
    }

    /**
     * @param PostStoreRequest $request
     * @return PostResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(PostStoreRequest $request)
    {
        $this->authorize('posts.create');
        if ($request->hasFile('thumbnail')) {
            $filename = $request->file('thumbnail')->getClientOriginalName();
            info($filename);
        }
        $post =  Post::create($request->validated());
        //sleep(2);

        return PostResource::make($post);
    }

    /**
     * @param Post $post
     * @return PostResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Post $post)
    {
        $this->authorize('posts.update');
        return PostResource::make($post);
    }

    /**
     * @param PostStoreRequest $request
     * @param Post $post
     * @return PostResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(PostStoreRequest $request, Post $post)
    {
        $this->authorize('posts.update');
        $post->update($request->validated());

        return PostResource::make($post);
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Post $post)
    {
        $this->authorize('posts.delete');
        $post->delete();
        return response()->noContent();
    }
}
